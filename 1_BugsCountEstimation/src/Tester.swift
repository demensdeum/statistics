class Tester {
    
    private let errorFindProbability: Int
    private let name: String
    
    init(name: String, errorFindProbability: Int) {
        
        guard name.count > 0 else { 
            fatalError("Testers must have names") 
        }
        
        guard errorFindProbability >= 0, errorFindProbability <= 100 else {
            fatalError("Error find probability must be in range 0 - 100%")
        }
        
        self.errorFindProbability = errorFindProbability
        self.name = name
        
        debugPrint("Tester with name \(name) and error find probability \(errorFindProbability)% was created")   
        
    }
    
    public func findBugs(in application: Application) -> [Bug] {
        
        var foundBugs = Set<Bug>()
        
        for _ in 0...application.bugsCount {
            
            let searchResult = Int.random(in: 0...100)
        
            let bugFound = searchResult <= errorFindProbability
        
            if bugFound {
        
                let bug = application.randomBug() 
        
                //debugPrint("Tester \(name) found a bug \(bug.name)")
        
                foundBugs.insert(bug)
            
            }            
            
        }
        
        return Array(foundBugs)
        
    }
}
