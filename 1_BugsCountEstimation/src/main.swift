print("\n\nBugs Estimation by Lincoln Index by demensdeum (demensdeum@gmail.com) 2018\n\n")

let aliceErrorFindProbability = 20
let bobErrorFindProbability = 60
let actualBugsCount = 200

Simulator.simulate(aliceErrorFindProbability: aliceErrorFindProbability, 
			bobErrorFindProbability: bobErrorFindProbability, 
				actualBugsCount: actualBugsCount)