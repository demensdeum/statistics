class Simulator {

	static func simulate(aliceErrorFindProbability: Int, bobErrorFindProbability: Int, actualBugsCount: Int) {
	
		// Programming process

		let application = Application(name: "Best Corporate Shiny App")

		for bugIndex in 0..<actualBugsCount {

		    let bug = Bug(name: "Bug \(bugIndex)", 
				  description: "Bug \(bugIndex) description")
                    
			application.add(bug: bug)
		}

		// QA process

		let testerAlice = Tester(name: "Alice", errorFindProbability: aliceErrorFindProbability)
		let testerBob = Tester(name: "Bob", errorFindProbability: bobErrorFindProbability)

		let bugsFoundedByAlice = testerAlice.findBugs(in: application)
		let bugsFoundedByBob = testerBob.findBugs(in: application)

		debugPrint("Alice found \(bugsFoundedByAlice.count) bugs")
		debugPrint("Bob found \(bugsFoundedByBob.count) bugs")

		// Lincoln Index

		let commonBugs = bugsFoundedByAlice.filter(bugsFoundedByBob.contains)

		guard commonBugs.count > 0 else {

		    fatalError("No common bugs found, one of testers must be lazy") 
    
		}

		debugPrint("Common bugs count: \(commonBugs.count)")

		let lincolnIndex = (bugsFoundedByAlice.count * bugsFoundedByBob.count) / commonBugs.count
		let bugsCount = application.bugsCount

		print("---")
		print("Estimation bugs count: \(lincolnIndex)")
		print("Actual bugs count: \(bugsCount)")	
	
	}

}
