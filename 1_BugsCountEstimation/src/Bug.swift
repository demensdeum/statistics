class Bug: Hashable {

    public let name: String
    public let description: String
    
    init(name: String, description: String) {
        
        guard name.count > 0 else {
        
            fatalError("Bug should contain name")
            
        }
        
        guard description.count > 0 else {
        
            fatalError("Bug should contain description")
            
        }
        
        self.name = name
        self.description = description
        
    }
    
    // MARK: Hashable

    static func == (lhs: Bug, rhs: Bug) -> Bool {
    
        return lhs.name == rhs.name && lhs.description == rhs.description
        
    }

    func hash(into hasher: inout Hasher) {
    
        hasher.combine(name)
        hasher.combine(description)
        
    }    
}
