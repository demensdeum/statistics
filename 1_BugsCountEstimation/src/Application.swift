class Application {

    private let name: String
    private var bugs = Set<Bug>()
    
    public var bugsCount: Int {
        return bugs.count
    }
    
    init(name: String) {
    
        guard name.count > 0 else {
        
            fatalError("Application should contain name")
            
        }
        
        self.name = name
        
    }
    
    public func add(bug: Bug) {
    
        bugs.insert(bug)
        
        //debugPrint("Bug with name: \"\(bug.name)\" and description \"\(bug.description)\" was added to application with name \"\(name)\"")
        
    }
    
    public func randomBug() -> Bug {
        
        guard bugs.count > 0, let bug = bugs.randomElement() else {
        
            fatalError("Application should contain bugs!")
            
        }
        
        return bug
        
    }
}
