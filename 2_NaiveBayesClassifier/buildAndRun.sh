shopt -s extglob

rm -rf build
mkdir build

cd src
cat !(main).swift > ../build/bayes.swift
cat main.swift >> ../build/bayes.swift
cd ..

swift build/bayes.swift
