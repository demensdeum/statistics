import Foundation

var date = Date()
let spammerUser = User(name: "Fione", friends: [], posts: [], registrationDate: date)

date.addDays(-60)
let notSpammerUser = User(name: "Boomer", friends: [], posts: [], registrationDate: date) 

let classifier = NaiveBayesClassifier()
let isFioneSpammer = classifier.isSpam(user: spammerUser)
let isBoomerSpammer = classifier.isSpam(user: notSpammerUser)

print("User \(spammerUser.name) is spammer: \(isFioneSpammer)")
print("User \(notSpammerUser.name) is spammer: \(isBoomerSpammer)")
