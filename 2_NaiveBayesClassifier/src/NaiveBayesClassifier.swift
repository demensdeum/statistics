class NaiveBayesClassifier: SpamUserClassifier {

    func isSpam(user: User) -> Bool {
    
        let userFriendRequestIsSpammerProbability = recentRegisteredUserIsSpammerProbability(user: user)
        let result = userFriendRequestIsSpammerProbability > 0.5
        print(userFriendRequestIsSpammerProbability)
        return result
    }

    // MARK: - Private
    
    private func recentRegisteredUserIsSpammerProbability(user: User) -> Float {
        
        guard let daysSinceNow = Date().daysSince(date: user.registrationDate) else { fatalError("Can't get Date since now") }
    
        let recentRegistrationDays = 30 * 3
        let daysInThreeMonths = recentRegistrationDays - daysSinceNow
        
        var userFriendRequestIsSpamProbability: Float = 1.0
        if daysInThreeMonths <= 0 {
            userFriendRequestIsSpamProbability = 0
        }
        else {
            userFriendRequestIsSpamProbability = Float(daysInThreeMonths) / Float(recentRegistrationDays) 
        }
        
        let kAllFrequentRegisteredUsersAreSpammersProbability: Float = 0.7 // P(B)
        let kAllSpammersAreFrequentlyRegisteredProbability: Float = 0.8 // P(B/A)
        
        let userFriendRequestIsSpammerProbability = (kAllSpammersAreFrequentlyRegisteredProbability * userFriendRequestIsSpamProbability) / kAllFrequentRegisteredUsersAreSpammersProbability // P(A/B)
        
        return userFriendRequestIsSpammerProbability
    }
}
