import Foundation

extension Date {

    func daysSince(date: Date) -> Int? {
        return Calendar.current.dateComponents([.day], from: date, to: self).day
    }

    mutating func addDays(_ days: Int) {
        guard let date = Calendar.current.date(byAdding: .day, value: days, to: self) else { fatalError("Can't change days") }
        self = date
    }    
    
}
