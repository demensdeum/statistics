protocol SpamUserClassifier {
    func isSpam(user: User) -> Bool
}
