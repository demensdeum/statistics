import Foundation

struct User {

    let name: String
    let friends: [User]
    let posts: [Post]
    let registrationDate: Date
    
}
