import Foundation

extension Date {

    func daysSince(date: Date) -> Int? {
        return Calendar.current.dateComponents([.day], from: date, to: self).day
    }

    mutating func addDays(_ days: Int) {
        guard let date = Calendar.current.date(byAdding: .day, value: days, to: self) else { fatalError("Can't change days") }
        self = date
    }    
    
}
class NaiveBayesClassifier: SpamUserClassifier {

    func isSpam(user: User) -> Bool {
    
        let userFriendRequestIsSpammerProbability = recentRegisteredUserIsSpammerProbability(user: user)
        let result = userFriendRequestIsSpammerProbability > 0.5
        print(userFriendRequestIsSpammerProbability)
        return result
    }

    // MARK: - Private
    
    private func recentRegisteredUserIsSpammerProbability(user: User) -> Float {
        
        guard let daysSinceNow = Date().daysSince(date: user.registrationDate) else { fatalError("Can't get Date since now") }
    
        let recentRegistrationDays = 30 * 3
        let daysInThreeMonths = recentRegistrationDays - daysSinceNow
        
        var userFriendRequestIsSpamProbability: Float = 1.0
        if daysInThreeMonths <= 0 {
            userFriendRequestIsSpamProbability = 0
        }
        else {
            userFriendRequestIsSpamProbability = Float(daysInThreeMonths) / Float(recentRegistrationDays) 
        }
        
        let kAllFrequentRegisteredUsersAreSpammersProbability: Float = 0.7 // P(B)
        let kAllSpammersAreFrequentlyRegisteredProbability: Float = 0.8 // P(B/A)
        
        let userFriendRequestIsSpammerProbability = (kAllSpammersAreFrequentlyRegisteredProbability * userFriendRequestIsSpamProbability) / kAllFrequentRegisteredUsersAreSpammersProbability // P(A/B)
        
        return userFriendRequestIsSpammerProbability
    }
}
import Foundation

struct Post {

    let creationDate: Date
    let message: String

}
protocol SpamUserClassifier {
    func isSpam(user: User) -> Bool
}
import Foundation

struct User {

    let name: String
    let friends: [User]
    let posts: [Post]
    let registrationDate: Date
    
}
import Foundation

var date = Date()
let spammerUser = User(name: "Fione", friends: [], posts: [], registrationDate: date)

date.addDays(-60)
let notSpammerUser = User(name: "Boomer", friends: [], posts: [], registrationDate: date) 

let classifier = NaiveBayesClassifier()
let isFioneSpammer = classifier.isSpam(user: spammerUser)
let isBoomerSpammer = classifier.isSpam(user: notSpammerUser)

print("User \(spammerUser.name) is spammer: \(isFioneSpammer)")
print("User \(notSpammerUser.name) is spammer: \(isBoomerSpammer)")
